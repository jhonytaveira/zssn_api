class Item < ActiveRecord::Base
  attr_accessible :name, :point

  belongs_to :survivor

  validates :name, :point, presence: true

  scope :by_name, -> value { where(name: value) }
  scope :by_infected_survivor, -> { joins { survivor }.merge(Survivor.by_infected) }
  scope :by_ids, -> value { where(id: value) }
end
