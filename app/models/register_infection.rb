class RegisterInfection < ActiveRecord::Base
  attr_accessible :infected_id, :informant_id

  belongs_to :infected, class_name: 'Survivor'
  belongs_to :informant, class_name: 'Survivor'

  after_create :mark_infected

  validates :infected_id, :informant_id, presence: true
  validates :informant_id, uniqueness: { scope: :infected_id }

  validate :check_informant

  private

  def mark_infected
    return if infected.register_infections.size < 3

    infected.mark!
  end

  def check_informant
    errors.add(:base, :the_informant_can_be_infected) if informant.try(:infectious?)
  end
end
