class Survivor < ActiveRecord::Base
  attr_accessible :name, :age, :gender, :infectious

  has_one :location

  has_many :items
  has_many :register_infections, foreign_key: :infected_id

  validates :name, :age, :gender, presence: true

  scope :by_infected, -> value= true { where(infectious: value) }
  scope :by_item_name, -> value { joins { items }.merge(Item.by_name(value)) }
  scope :by_ids, -> value { where(id: value) }

  def mark!
    self.infectious = true
    self.save!
  end
end
