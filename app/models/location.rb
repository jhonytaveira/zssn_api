class Location < ActiveRecord::Base
  attr_accessible :longitude, :latitude

  belongs_to :survivor

  validates :longitude, :latitude, presence: true
end
