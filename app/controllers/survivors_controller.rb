class SurvivorsController < ApplicationController
  def create
    @survivor = Survivor.new(params[:survivor])

    @survivor.transaction do
      LocationMaker.create! @survivor, params[:survivor][:location]
      ItemMaker.create! @survivor, params[:survivor][:items]

      render json: { survivor: @survivor, location: @survivor.location, items: @survivor.items }
    end
  end
end
