class Reports::SurvivorReportsController < ApplicationController
  def percentage_infected
    render json: { percentage: SurvivorReport.percentage! }
  end

  def percentage_uninfected
    render json: { percentage: SurvivorReport.percentage!(infected: false) }
  end

  def resource_average
    render json: { average: SurvivorReport.resource_average! }
  end

  def survivor_lost_point
    render json: { lost_points: SurvivorReport.survivor_lost_point! }
  end
end
