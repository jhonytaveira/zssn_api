class LocationsController < ApplicationController
  def update
    @location = Location.find params[:id]

    if @location.update params[:location]
      render json: @location
    else
      render json: { errors: @location.errors }
    end
  end
end
