class RegisterInfectionsController < ApplicationController
  def create
    @register_infection = RegisterInfection.new params[:register_infection]

    if @register_infection.save!
      render json: @register_infection
    else
      render json: { errors: @register_infection.errors }
    end
  end
end
