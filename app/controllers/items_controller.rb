class ItemsController < ApplicationController
  def survivor_exchange_item
    if SurvivorExchangeItemMaker.do! params[:items]
      render json: { messages: I18n.t('messages.success.successfully_exchanged_items') }
    end
  end
end
