class SurvivorExchangeItemMaker
  def initialize(items, options={})
    @items = items
    @survivor_ids = items.keys
    @item_repository = options.delete(:item_repository) { Item }
    @survivor_repository = options.delete(:survivor_repository) { Survivor }
  end

  def self.do!(*attr)
    new(*attr).do!
  end

  def do!
    raise 'As pontuções dos itens á ser trocados devem ser iguais.' if check_points_items

    raise 'Sobreviventes infectados não devem fazer negociação.' if infected?

    exchange_items

    items.each do |survivor_id, item_ids|
      item_repository.by_ids(item_ids).update_all(survivor_id: survivor_id)
    end
  end

  private

  attr_reader :items, :survivor_ids, :item_repository, :survivor_repository

  # Troca os itens entre os sobreviventes
  def exchange_items
    temp = items[survivor_ids.first]

    items[survivor_ids.first] = items[survivor_ids.second]
    items[survivor_ids.second] = temp
  end

  # Verifica se as pontuções dos itens á ser trocados são iguais
  def check_points_items
    points = items.collect { |_, ids| item_repository.by_ids(ids).to_a.sum(&:point) }

    points.first != points.last
  end

  # Verifica se existe algum sobrevivente infectado
  def infected?
    survivor_repository.by_ids(survivor_ids).by_infected.any?
  end
end
