class ItemMaker
  def initialize(survivor, params, options ={})
    @survivor = survivor
    @params = params
    @item_repository = options.delete(:item_repository) { Item }
  end

  def self.create!(*attr)
    new(*attr).create!
  end

  # Cadastra os itens do sobrevivente
  def create!
    params.each do |_, key|
      item = item_repository.new

      item.survivor = survivor
      item.name = key[:name]
      item.point = key[:point]
      item.save!
    end
  end

  private

  attr_reader :item_repository, :survivor, :params
end
