class LocationMaker
  def initialize(survivor, params, options ={})
    @survivor = survivor
    @params = params
    @location_repository = options.delete(:location_repository) { Location }
  end

  def self.create!(*attr)
    new(*attr).create!
  end

  # Cadastra o local do sobrevivente
  def create!
    location = location_repository.new

    location.survivor = survivor
    location.latitude = params[:latitude]
    location.longitude = params[:longitude]

    location.save!
  end

  private

  attr_reader :location_repository, :survivor, :params
end
