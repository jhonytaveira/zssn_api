class SurvivorReport
  def initialize(options= {})
    @infected = options.delete(:infected) { true }
    @survivor_repository = options.delete(:survivor_repository) { Survivor }
    @item_repository = options.delete(:item_repository) { Item }
  end

  def self.percentage!(*attr)
    new(*attr).percentage!
  end

  def self.resource_average!(*attr)
    new(*attr).resource_average!
  end

  def self.survivor_lost_point!(*attr)
    new(*attr).survivor_lost_point!
  end

  # Retorna a porcentagem de infectados ou não infectados
  def percentage!
    total = infected ? infected_survivor.count.to_f : infected_survivor(false).count.to_f
    ((total / total_survivor.to_f) * 100).round(2)
  end

  # Retorna a média de recurso por sobrevivente
  def resource_average!
    uninfected_survivor = infected_survivor(infected=false).count
    records = {}

    item_repository.all.map(&:name).uniq.each do |name|
      items_qtt = item_repository.by_name(name).count
      average = (items_qtt.to_f / uninfected_survivor).round(2)
      records[name] = average
    end
    records
  end

  # Retorna o total de pontos perdidos dos sobreviventes infectados
  def survivor_lost_point!
    item_repository.by_infected_survivor.map(&:point).sum
  end

  private

  attr_reader :infected, :item, :survivor_repository, :item_repository

  # total de sobreviventes
  def total_survivor
    total = survivor_repository.count
    total.zero? ? 1 : total.to_f
  end

  # Retorna a coleção de infectados ou não infectados
  def infected_survivor(infected=true)
    survivor_repository.by_infected(infected)
  end
end
