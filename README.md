# API ZSSN (Zombie Survival Rede Social) #
* [Requisitos, pra desenvolvimento da API](https://gist.github.com/akitaonrails/711b5553533d1a14364907bbcdbee677)
## Resumo ##
**1 - Responsáveis por adicionar sobrevivente, registrando também seu local e items.**
`app/controllers/survivors_controller.rb`
`app/business/item_maker.rb`
`app/business/location_maker.rb`

**2 - Atualiza o local do sobreviente.**                                              
`app/controllers/locations_controller.rb`

**3 - Cria registro de infecção.**                                                  
`app/controllers/register_infections_controller.rb`

**4 - Faz trocas de items entre sobreviventes.**                     
`app/controllers/items_controller.rb`
`app/business/survivor_exchange_item_maker.rb`

**5 - Validações de acordo com os requisitos.**

### Gera 4 tipos de relatórios ###
   * Percentual de sobreviventes infectados.
   * Percentual de sobreviventes não infectados.
   * Quantidade média de cada tipo de recurso pelo sobrevivente (por exemplo, 5 águas por usuário).
   * Pontos perdidos por causa do sobrevivente infectado.               
  `app/controllers/reports/survivor_reports_controller.rb`
  `app/reports/survivor_report.rb`

### Como faço para configurar? ###
 **dependências**
 *mysql, 
 ruby 2.2 ou versão superior*

`bundle install`

`rake db:create`

`rake db:migrate`

### Execução de testes ###

`rspec spec/controller`

`rspec spec/integration/`

`rspec spec/models/`

`rspec spec/business/`

`rspec spec/reports/`