class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.decimal :latitude, precision: 14, scale: 6
      t.decimal :longitude, precision: 14, scale: 6
      t.references :survivor
    end
  end
end
