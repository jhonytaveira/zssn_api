class CreateRegisterInfections < ActiveRecord::Migration
  def change
    create_table :register_infections do |t|
      t.integer :infected_id, null: false
      t.integer :informant_id, null: false
    end
  end
end
