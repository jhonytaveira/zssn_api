Rails.application.routes.draw do

  resources :survivors, only: [:create] do
    resources :locations, only: [:update]
  end

  resources :items, only: [:create] do
    get :survivor_exchange_item, on: :collection
  end

  resources :register_infections, only: [:create]

  namespace :reports do
    resources :survivor_reports do
      collection do
        get :percentage_infected
        get :percentage_uninfected
        get :resource_average
        get :survivor_lost_point
      end
    end
  end
end
