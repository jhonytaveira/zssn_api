require File.expand_path('../boot', __FILE__)

require "rails-api"
require "protected_attributes"
require "active_record/railtie"

module ZssnApi
  class Application < Rails::Application
    config.autoload_paths += %W(
      #{config.root}/app/business
      #{config.root}/app/reports
    )
    config.i18n.default_locale = :'pt-BR'
    config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
  end
end
