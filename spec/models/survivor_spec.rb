require 'model_helper'
require 'app/models/survivor'
require 'app/models/location'
require 'app/models/item'

describe Survivor do
  it { is_expected.to have_one(:location) }

  it { is_expected.to have_many(:items) }
  it { is_expected.to have_many(:register_infections).with_foreign_key(:infected_id) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:age) }
  it { is_expected.to validate_presence_of(:gender) }

  describe '#mark!' do
    let(:survivor){ {name: 'joão', age: 35, gender: 'masculino', infectious: false } }
    it '#save' do
      allow(subject).to receive_messages(survivor)
      expect(subject).to receive(:infectious=).with(true)
      expect(subject).to receive(:save!)
      subject.mark!
    end
  end
end
