require 'model_helper'
require 'app/models/survivor'

describe RegisterInfection do
  it { is_expected.to belong_to(:infected).class_name('Survivor') }
  it { is_expected.to belong_to(:informant).class_name('Survivor') }

  it { is_expected.to validate_presence_of(:infected_id) }
  it { is_expected.to validate_presence_of(:informant_id) }
end
