require 'model_helper'
require 'app/models/survivor'
require 'app/models/location'

describe Location do
  it { is_expected.to belong_to(:survivor) }

  it { is_expected.to validate_presence_of(:latitude) }
  it { is_expected.to validate_presence_of(:longitude) }
end
