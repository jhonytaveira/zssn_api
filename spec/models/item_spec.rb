require 'model_helper'
require 'app/models/survivor'
require 'app/models/item'

describe Item do
  it { is_expected.to belong_to(:survivor) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:point) }
end
