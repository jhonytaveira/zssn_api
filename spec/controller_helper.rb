require 'spec_helper'

RSpec.configure do |config|
  config.include RSpec::Rails::ControllerExampleGroup
  config.include JsonHelpers, type: :controller
end
