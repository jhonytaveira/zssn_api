require 'unit_helper'
require 'app/business/item_maker'

describe ItemMaker do
  let(:survivor) { double(:survivor) }
  let(:item) { double(:item, survivor: survivor) }
  let(:params) { { 0 => { name: 'water', point: 4 }, 1 => { name: 'ammunition', point: 1} } }
  let(:item_repository) { double(:item_repository) }

  subject do
    described_class.new(
      survivor,
      params,
      item_repository: item_repository
    )
  end

  it 'creates a new item' do
    expect(item_repository).to receive(:new).and_return(item).exactly(2).times

    expect(item).to receive(:survivor=).with(survivor).exactly(2).times
    expect(item).to receive(:name=).with('water')
    expect(item).to receive(:point=).with(4)
    expect(item).to receive(:name=).with('ammunition')
    expect(item).to receive(:point=).with(1)
    expect(item).to receive(:save!).exactly(2).times

    subject.create!
  end
end
