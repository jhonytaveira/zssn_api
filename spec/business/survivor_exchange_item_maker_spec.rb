require 'unit_helper'
require 'app/business/survivor_exchange_item_maker'

describe SurvivorExchangeItemMaker do
  let(:survivor_ids) { double(:survivor_ids) }
  let(:survivor) { double(:survivor) }
  let(:item_1) { double(:item_1, id: 1, name: 'water', point: 4, survivor_id: 1) }
  let(:item_2) { double(:item_2, id: 2, name: 'ammunition', point: 2, survivor_id: 2) }
  let(:item_3) { double(:item_4, id: 1, name: 'water', point: 3, survivor_id: 1) }
  let(:item_4) { double(:item_4, id: 2, name: 'ammunition', point: 3, survivor_id: 2) }
  let(:items) { { 1 => [1], 2 => [2] } }
  let(:item_repository) { double(:item_repository) }
  let(:survivor_repository) { double(:survivor_repository) }

  subject do
    described_class.new(
      items,
      survivor_ids: survivor_ids,
      item_repository: item_repository,
      survivor_repository: survivor_repository
    )
  end

  describe '#do!' do
    context 'The scores of the items are not equal' do
      it 'raises error' do
        expect(item_repository).to receive(:by_ids).and_return([item_1])
        expect(item_repository).to receive(:by_ids).and_return([item_2])
        expect do
          subject.do!
        end.to raise_error(RuntimeError, 'As pontuções dos itens á ser trocados devem ser iguais.')
      end
    end

    context 'The scores of the items are not equal' do
      it 'raises error' do
        expect(item_repository).to receive(:by_ids).and_return([item_3])
        expect(item_repository).to receive(:by_ids).and_return([item_4])
        expect(survivor_repository).to receive(:by_ids).and_return(survivor)
        expect(survivor).to receive(:by_infected).and_return([survivor])

        expect do
          subject.do!
        end.to raise_error(RuntimeError, 'Sobreviventes infectados não devem fazer negociação.')
      end
    end

    context 'When the points of the items are equal and uninfected survivors' do
      it 'returns to exchange items' do
        expect(item_repository).to receive(:by_ids).and_return([item_3])
        expect(item_repository).to receive(:by_ids).and_return([item_4])
        expect(item_repository).to receive(:by_ids).and_return(item_2)
        expect(item_repository).to receive(:by_ids).and_return(item_1)
        expect(survivor_repository).to receive(:by_ids).and_return(survivor)
        expect(survivor).to receive(:by_infected).and_return([])
        expect(item_2).to receive(:update_all).with({:survivor_id => 1 })
        expect(item_1).to receive(:update_all).with({:survivor_id => 2 })
        subject.do!
      end
    end
  end
end
