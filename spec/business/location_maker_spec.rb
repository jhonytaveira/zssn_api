require 'unit_helper'
require 'app/business/location_maker'

describe LocationMaker do
  let(:survivor) { double(:survivor) }
  let(:location) { double(:location, survivor: survivor) }
  let(:params) { { latitude: 12354.56, longitude: 34533.67 } }
  let(:location_repository) { double(:location_repository) }

  subject do
    described_class.new(
      survivor,
      params,
      location_repository: location_repository
    )
  end

  it 'creates a new location' do
    expect(location_repository).to receive(:new).and_return(location)

    expect(location).to receive(:survivor=).with(survivor)
    expect(location).to receive(:latitude=).with(12354.56)
    expect(location).to receive(:longitude=).with(34533.67)
    expect(location).to receive(:save!)

    subject.create!
  end
end
