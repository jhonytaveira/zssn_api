ENV['RAILS_ENV'] ||= 'test'
require 'rubygems'
require 'bundler/setup'

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

$:.unshift File.expand_path('../../', __FILE__)

require 'active_support/dependencies/autoload'
require 'active_support/core_ext'
