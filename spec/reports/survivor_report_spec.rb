require 'unit_helper'
require 'app/reports/survivor_report'

describe SurvivorReport do
  let(:infected) { (true) }
  let(:survivor_repository) { [survivor_1, survivor_2, survivor_3, survivor_4] }
  let(:item_repository) { double(:item_repository) }
  let(:survivor_1) { double(:survivor_1, infectious: true) }
  let(:survivor_2) { double(:survivor_2, infectious: false) }
  let(:survivor_3) { double(:survivor_3, infectious: false) }
  let(:survivor_4) { double(:survivor_4, infectious: false) }

  let(:item_1) { double(:item1, name: 'medication', point: 2, survivor_id: 1) }
  let(:item_2) { double(:item2, name: 'water', point: 4) }
  let(:item_3) { double(:item3, name: 'food', point: 3) }
  let(:item_4) { double(:item4, name: 'ammunition', point: 1) }
  let(:item_5) { double(:item5, name: 'ammunition', point: 2) }
  let(:item_6) { double(:item6, name: 'ammunition', point: 1) }

  subject do
    described_class.new(
      infected: infected,
      survivor_repository: survivor_repository,
      item_repository: item_repository
    )
  end

  context 'Should calculate the percentage of infected survivors' do
    it 'Returns percentage' do
      expect(survivor_repository).to receive(:by_infected).with(true).and_return([survivor_1])
      expect(subject.percentage!).to eq 25.0
    end
  end

  context 'Should calculate the percentage of uninfected survivors' do
    let(:infected) { (false) }
    it 'Returns percentage' do
      expect(survivor_repository).to receive(:by_infected).with(false).and_return([survivor_2, survivor_3, survivor_4 ])
      expect(subject.percentage!).to eq 75.0
    end
  end

  context 'Should calculate the average feature by survivor' do
    it 'returns average resources' do
      expect(survivor_repository).to receive(:by_infected).with(false).and_return([survivor_2, survivor_3, survivor_4 ])
      expect(item_repository).to receive(:all).and_return([item_1, item_2, item_3, item_4])
      expect(item_repository).to receive(:by_name).and_return([item_1])
      expect(item_repository).to receive(:by_name).and_return([item_2])
      expect(item_repository).to receive(:by_name).and_return([item_3])
      expect(item_repository).to receive(:by_name).and_return([item_4, item_5, item_6])

      expect(subject.resource_average!).to eq (
        { "medication" => 0.33, "water" => 0.33, "food" => 0.33, "ammunition" => 1.0 }
      )
    end
  end

  context 'Should get the point total of the infected survivors' do
    it 'returns total points' do
      expect(item_repository).to receive(:by_infected_survivor).and_return([item_1])
      expect(subject.survivor_lost_point!).to eq 2
    end
  end
end
