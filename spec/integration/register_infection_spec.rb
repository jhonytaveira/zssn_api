require 'spec_helper'

describe RegisterInfection do
  subject { FactoryGirl.cache(:register_infection) }

  context 'validations' do
    it { is_expected.to validate_uniqueness_of(:informant_id).scoped_to(:infected_id) }
  end

  context 'When the informant survivor is infected' do
    it 'Should not create registry infection' do
      survivor_informant = FactoryGirl.cache(:survivor_jose, infectious: true)
      register_infection = FactoryGirl.build(:register_infection, informant: survivor_informant)

      register_infection.valid?
      expect(register_infection.errors[:base]).to include 'O informante não deve estar infectado.'
    end
  end

  describe '#mark_infected' do
    context 'When the survivor has three records of infection or more' do
      it 'Should change the status to infected' do
        survivor = FactoryGirl.cache(:survivor, infectious: false)
        survivor_maria = FactoryGirl.cache(:survivor_maria)
        survivor_jose = FactoryGirl.cache(:survivor_jose)
        survivor_tiago = FactoryGirl.cache(:survivor_tiago)

        FactoryGirl.cache(:register_infection, informant: survivor_maria, infected: survivor)
        FactoryGirl.cache(:register_infection, informant: survivor_jose, infected: survivor)
        FactoryGirl.cache(:register_infection, informant: survivor_tiago, infected: survivor)

        expect(survivor.infectious).to eq true
      end
    end
  end
end
