ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'factory_girl_rails'
require 'rails-api'
require 'database_cleaner'
require 'active_record'
require 'protected_attributes'
require 'shoulda-matchers'
require 'squeel'

Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}
