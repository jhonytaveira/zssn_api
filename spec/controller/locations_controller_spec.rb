require "controller_helper"

describe LocationsController, type: :controller do
  describe 'PUT #location' do
    context 'update the location of the survivor' do
      it 'return location' do
        location = FactoryGirl.cache(:location)
        survivor_id = location.survivor_id

        put :update, { format: :json, id: location.id, survivor_id: survivor_id,
          location: { longitude: 34556.23, latitude: 6544333 } }

        expect(json).to eq(
          {
            "id" => location.id, "latitude" => "6544333.0", "longitude" => "34556.23", "survivor_id" => location.survivor_id
          }
        )
      end
    end
  end
end
