require "controller_helper"

describe RegisterInfectionsController, type: :controller do
  describe 'POST #register_infection' do
    context 'create registry infection' do
      it 'return register_infection' do
        survivor = FactoryGirl.cache(:survivor)
        survivor_second = FactoryGirl.cache(:survivor_maria)

        post :create, register_infection: { informant_id: survivor.id, infected_id: survivor_second.id }

        expect(json).to eq(
          {
            "id" => 1, "infected_id" => survivor_second.id, "informant_id" => survivor.id
          }
        )
      end
    end
  end
end
