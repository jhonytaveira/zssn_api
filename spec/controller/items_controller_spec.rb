require "controller_helper"

describe ItemsController, type: :controller do
  describe 'GET #item' do
    context 'exchange items among survivors' do
      it 'return items' do
        item_first = FactoryGirl.cache(:item)
        item_second = FactoryGirl.cache(:item_ammunition)
        item_third = FactoryGirl.cache(:item_medication)
        item_fourth = FactoryGirl.cache(:item_food)

        survivor_maria = item_first.survivor
        survivor_jose = item_fourth.survivor

        items_maria = survivor_maria.items
        items_jose =  survivor_jose.items

        # Os items antes da troca entre sobreviventes
        expect(items_maria.first.name).to eq 'water'
        expect(items_maria.last.name).to eq 'ammunition'
        expect(items_jose.first.name).to eq 'medication'
        expect(items_jose.last.name).to eq 'food'

        items = { survivor_maria.id =>  [item_first.id, item_second.id],
                  survivor_jose.id => [item_third.id, item_fourth.id]
                }

        get :survivor_exchange_item, { format: :json, items: items }

        # Os items depois da troca entre sobreviventes
        expect(items_maria.first.name).to eq 'medication'
        expect(items_maria.last.name).to eq 'food'
        expect(items_jose.first.name).to eq 'water'
        expect(items_jose.last.name).to eq 'ammunition'

        expect(json['messages']).to eq 'itens trocados com sucesso'
      end
    end
  end
end
