require "controller_helper"

describe Reports::SurvivorReportsController, type: :controller do
  describe 'GET #percentage_infected' do
    context 'survivor report infected' do
      it 'return percentage of infected' do
        FactoryGirl.cache(:survivor, infectious: true)
        FactoryGirl.cache(:survivor, infectious: false)

        get :percentage_infected, { format: :json }

        expect(json).to eq(
          { "percentage" => 50.0 }
        )
      end
    end
  end

  describe 'GET #percentage_uninfected' do
    context 'survivor report uninfected' do
      it 'return percentage of uninfected' do
        FactoryGirl.cache(:survivor, infectious: true)
        FactoryGirl.cache(:survivor, infectious: false)
        FactoryGirl.cache(:survivor, name: 'José', infectious: false)

        get :percentage_uninfected, { format: :json }

        expect(json).to eq(
          { "percentage" => 66.67 }
        )
      end
    end
  end

  describe 'GET #resource_average' do
    context 'average resource for survivor' do
      it 'return average resource for survivor' do
        FactoryGirl.cache(:item_food)
        FactoryGirl.cache(:item_ammunition)
        FactoryGirl.cache(:item_ammunition_second)

        get :resource_average, { format: :json }

        expect(json).to eq(
          { "average" => { "food" => 0.5, "ammunition" => 1.0 } }
        )
      end
    end
  end

  describe 'GET #survivor_lost_point' do
    context 'points lost from infected survivors' do
      it 'returns the total points lost' do
        survivor = FactoryGirl.cache(:survivor, infectious: true)
        FactoryGirl.cache(:item_food, survivor: survivor)
        FactoryGirl.cache(:item_ammunition, survivor: survivor)
        FactoryGirl.cache(:item_ammunition_second, survivor: survivor)

        get :survivor_lost_point, { format: :json }

        expect(json).to eq(
          { "lost_points" => 6 }
        )
      end
    end
  end
end
