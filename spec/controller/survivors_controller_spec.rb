require "controller_helper"

describe SurvivorsController, type: :controller do
  describe 'POST #survivor' do
    context 'create survivor, location and items' do
      it 'return survivor, location and items' do

        survivor = { name: 'jhony', age: 25, gender: 'masculino', location: { latitude: 123.85, longitude: 234.67 },
          items: { '0' => { name: 'watter', point: 4 } } }

        post :create, survivor: survivor

        survivor_id = Survivor.last.id
        location_id = Location.last.id
        item_first_id = Item.first.id

        expect(json).to eq(
          { "survivor" => { "id" => survivor_id , "name"=>"jhony", "age" => 25, "gender" => "masculino", "infectious" => false },

              "location" => { "id" => location_id, "latitude" => "123.85", "longitude" => "234.67", "survivor_id" => survivor_id },

              "items" => [ { "id" => item_first_id, "name" => "watter", "point" => 4, "survivor_id" => survivor_id } ]
          }
        )
      end
    end
  end
end
