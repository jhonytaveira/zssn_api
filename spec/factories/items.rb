FactoryGirl.define do
  factory :item do
    name 'water'
    point '4'
    survivor { FactoryGirl.cache(:survivor_maria) }

    factory :item_ammunition do
      name 'ammunition'
      point '1'
    end

    factory :item_ammunition_second do
      name 'ammunition'
      point '2'
    end

    factory :item_food do
      survivor { FactoryGirl.cache(:survivor_jose) }
      name 'food'
      point '3'
    end

    factory :item_medication do
      name 'medication'
      point '2'
      survivor { FactoryGirl.cache(:survivor_jose) }
    end
  end
end
