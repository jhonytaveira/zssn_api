FactoryGirl.define do
  factory :location do
    latitude 123.45
    longitude 346.67
    survivor { FactoryGirl.cache(:survivor) }
  end
end
