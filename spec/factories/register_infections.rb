FactoryGirl.define do
  factory :register_infection do
    informant { FactoryGirl.cache(:survivor_maria) }
    infected { FactoryGirl.cache(:survivor_jose) }
  end
end
