FactoryGirl.define do
  factory :survivor do
    name 'marcos'
    age 28
    gender 'masculino'

    factory :survivor_maria do
      name 'maria'
      age 25
      gender 'feminino'
    end

    factory :survivor_jose do
      name 'josé'
      age 27
      gender 'masculino'
    end

    factory :survivor_tiago do
      name 'tiago'
      age 23
      gender 'masculino'
    end
  end
end
